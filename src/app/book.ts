export default class Book {
  id: number;
  isbn: string;
  title: string;
}
