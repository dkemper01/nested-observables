import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams }from '@angular/common/http';
import { throwError, forkJoin } from 'rxjs';
import Book from '../book';
import Retailer from '../retailer';

@Injectable({
  providedIn: 'root'
})
export class RetailerService {
  retailersUrl = 'api/retailers';  // URL to retailers web api
  booksUrl = 'api/books';  // URL to books web api

  constructor(private http: HttpClient) { }

  joinBooksAndRetailers(): void
  {
    const retailersSourceObservable = this.http.get<Retailer[]>(this.retailersUrl);
    const booksSourceObservable = this.http.get<Book[]>(this.booksUrl);

    forkJoin([retailersSourceObservable, booksSourceObservable]).subscribe(results => {
      const [retailers, books] = results;
      console.table(retailers);
      console.table(books);
    });
  }

  private handleError (error: any) {
    // In a real world app, we might send the error to remote logging infrastructure
    // and reformat for user consumption
    console.error(error); // log to console instead
    return throwError(error);
  }
}
