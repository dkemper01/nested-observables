import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams }from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { tap, catchError, map, mergeMap } from 'rxjs/operators';
import Book from '../book';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  booksUrl = 'api/books';  // URL to web api

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]>
  {
    return this.http.get<Book[]>(this.booksUrl).pipe(
      mergeMap(data => { 
        console.log(data.map(b => b.id));
        return this.http.get<Book[]>(this.booksUrl)
      }), 
      catchError(this.handleError),
      mergeMap(data => { 
        console.log(data.map(b => b.isbn));
        return of(data);
      }), 
      catchError(this.handleError),
      mergeMap(data => { 
        const mappedTitle = data.map(b => b.title);
        console.log(mappedTitle);
        return of(data);
      }), 
      catchError(this.handleError)
    );
  }

  private handleError (error: any) {
    // In a real world app, we might send the error to remote logging infrastructure
    // and reformat for user consumption
    console.error(error); // log to console instead
    return throwError(error);
  }
}
