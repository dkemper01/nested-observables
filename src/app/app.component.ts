import { Component, OnInit } from '@angular/core';
import { BookService } from './services/book.service';
import { RetailerService } from './services/retailer.service';
import { faTerminal } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  faTerminal = faTerminal;
  
  constructor(private bookService: BookService, private retailerService: RetailerService) { }

  ngOnInit(): void {
    this.bookService.getBooks().subscribe();
    this.retailerService.joinBooksAndRetailers();
  }
  
  title = 'nested observables demo';
}
