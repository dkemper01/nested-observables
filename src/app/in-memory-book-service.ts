import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryBookService implements InMemoryDbService {
  createDb(reqInfo?: import("angular-in-memory-web-api").RequestInfo): {} | import("rxjs").Observable<{}> | Promise<{}> {
    let books = [
      { id: 1, isbn: 100, title: 'Book 1' },
      { id: 2, isbn: 200, title: 'Book 2' },
      { id: 3, isbn: 300, title: 'Book 3' },
      { id: 4, isbn: 400, title: 'Book 4' }
    ];
    let retailers = [
      { id: 1, name: 'Barnes and Noble' },
      { id: 2, name: 'Amazon' },
      { id: 3, name: 'A Book Apart' },
      { id: 4, name: 'Dobbs' }
    ];
    
    return { books, retailers };
  }

}
